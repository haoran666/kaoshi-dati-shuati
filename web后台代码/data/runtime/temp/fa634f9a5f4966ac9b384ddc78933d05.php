<?php /*a:2:{s:92:"/www/wwwroot/dati.sdwanyue.com/public/themes/admin_simpleboot3/admin/partypay/statistic.html";i:1646978670;s:81:"/www/wwwroot/dati.sdwanyue.com/public/themes/admin_simpleboot3/public/header.html";i:1646978690;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>

<!--党费统计-->

<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo url('partypay/statistic'); ?>">党费统计</a></li>
        <!-- <li><a href="<?php echo url('partypay/add'); ?>">添加</a></li> -->
    </ul>
    <div class="top-form-wrapper margin-top-20 clearfix">
        <form style="float: left; border-right: none;" class="well form-inline" method="get" name="form1" action="<?php echo url('partypay/statistic'); ?>">
            缴费年月：
            <div class="form-group">
                <div class="col-md-6 col-sm-10">
                    <input type="month" value="<?php echo $current_month; ?>" class="form-control" id="input-date" name="year_month">
                </div>
            </div>
            是否缴费：
            <select class="form-control" name="status" id="">
                <?php if(is_array($status) || $status instanceof \think\Collection || $status instanceof \think\Paginator): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.status') != '' && input('request.status') == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
            <input type="submit" class="btn btn-primary" value="搜索" onclick="form1.action='<?php echo url('partypay/statistic'); ?>';form1.submit();">


        </form>

        <form style="border-left: none;" class="well form-inline" method="post" action="<?php echo url('admin/partypay/export'); ?>">
            <!--导出用户-->
            <input type="hidden" id="export_ymonth" name="export_ymonth" value="<?php echo $current_month; ?>">
            <button type="submit" class="btn btn-danger">导出缴费统计</button>
        </form>


    </div>

        <table class="table table-hover table-bordered margin-top-20">
            <thead>
            <tr>
                <th width="50">ID</th>
                <th>昵称</th>
                <th>应交党费</th>
                <th width="360"><?php echo lang('ACTIONS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php 

             if(is_array($user_list) || $user_list instanceof \think\Collection || $user_list instanceof \think\Paginator): if( count($user_list)==0 ) : echo "" ;else: foreach($user_list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['user_nickname']; ?></td>
                    <td><?php echo $vo['party_money']; ?></td>
                    <td>
                        <a class="btn btn-xs btn-success" href="javascript:parent.openIframeLayer('<?php echo url('partypay/userstatisc',array('id'=>$vo['id'])); ?>')">查看缴费历史</a>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>

            </tbody>
        </table>
    <div class="pagination"><?php echo $page; ?></div>
</div>
<script src="/static/js/admin.js"></script>
<script type="text/javascript" src="/static/js/layer/layer.js"></script>

<script type="text/javascript">

   $("#input-date").change(function () {
      $("#export_ymonth").val($(this).val());
   });

</script>


</body>
</html>